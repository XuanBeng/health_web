export interface DailyEntry {
  id?: string;
  user?: string;
  if_cough?: string;
  if_cough_n?: string;
  temperature?: string;
  breath_short?: string;
  breath_short_n?: string;
  other?: string;
  opdate?: Date;
}
