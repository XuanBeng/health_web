export interface Result<T> {
  errCode: number;
  errMessage: string;
  data: T;
}

export interface Content {
  id?: number;
  content?: any;
  date?: Date;
}
