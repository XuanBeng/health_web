import { environment } from 'src/environments/environment';

// export const baseUrl = 'http://127.0.0.1:8000/health/api/v1/';

const baseUrl = environment.baseUrl;

export const loginUrl = baseUrl + 'user/login/';
export const userUrl = baseUrl + 'user/user/';
export const daliyEntryUrl = baseUrl + 'app/daily-entry/';
export const unitUrl = baseUrl + 'app/unit/';
export const provincesUrl = baseUrl + 'app/provinces/';
export const newsUrl = baseUrl + 'app/news/';
