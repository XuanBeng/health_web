/**
 * 将json数组A中按照字符串B进行过滤
 * @param searchString 字符串B
 * @param object json数组A
 * @param list 需要过滤的属性列表，与mode配合使用 e.g. 'id', 'edit'
 * @param mode 过滤模式：0 包含;1 剔除, 默认为0
 */
export function _searchFilter(searchString: string, object: any, list: string[], mode: number = 0): any {
    let flag = false;
    for (const key in object) {
        if (object.hasOwnProperty(key)) {
            if (mode === 0) {
                // 包含
                if (list.indexOf(key) < 0) { continue; }
            } else if (mode === 1) {
                // 剔除
                if (list.indexOf(key) >= 0) { continue; }
            }
            try {
                if (object[key] && object[key].toString().indexOf(searchString) >= 0) {
                    flag = true;
                    break;
                }
            } catch {
            }
        }
    }
    return flag;
}
