import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import * as echarts from 'echarts';
import { switchMap, tap } from 'rxjs/operators';
import { Content } from 'src/app/models/result';
import { EpidemicService } from 'src/app/services/epidemic.service';

@Component({
  selector: 'app-epidemic',
  templateUrl: './epidemic.component.html',
  styleUrls: ['./epidemic.component.less']
})
export class EpidemicComponent implements OnInit {
  option = {};
  content: Content;
  timer;
  mapDataList: any[];
  displayProvincesList: any[];

  displayNewsList: any[];
  displayDesc: any;

  loading = false;

  // 统计数据
  currentConfirmedCount = 0;
  confirmedCount = 0;
  deadCount = 0;
  curedCount = 0;
  suspectedCount = 0;
  seriousCount = 0;

  constructor(
    private http: HttpClient,
    private epidemicService: EpidemicService
  ) {}

  ngOnInit() {
    this.getProvinces();
    this.getNews();
    this.setTimeRefresh();
  }

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnDestroy() {
    if (this.timer) {
      clearInterval(this.timer); // 销毁定时器
    }
  }

  setTimeRefresh() {
    this.timer = setInterval(() => {
      this.loadData();
    }, 43200000);
  }
  loadMap(mapData) {
    this.http.get('assets/china.json').subscribe(geoJson => {
      echarts.registerMap('China', geoJson);
      this.option = {
        tooltip: {
          trigger: 'item',
          formatter: '{b}：{c}'
        },
        toolbox: {
          show: true,
          orient: 'vertical',
          left: 'right',
          top: 'center',
          feature: {
            dataView: { readOnly: false },
            restore: {},
            saveAsImage: {}
          }
        },
        visualMap: {
          type: 'piecewise',
          pieces: [
            { min: 10000 },
            { min: 1000, max: 10000 },
            { min: 500, max: 999 },
            { min: 100, max: 499 },
            { min: 10, max: 99 },
            { min: 2, max: 9 },
            { max: 1 }
          ],
          color: ['#880000', '#FF0000', '#E6E6FA']
          // inRange: {
          //   color: ['write', 'red', 'black']
          // }
        },
        series: [
          {
            type: 'map',
            mapType: 'China', //  与注册时的名字保持统一   echarts.registerMap('China', geoJson);
            itemStyle: {
              normal: {
                areaColor: '#AAD5FF',
                borderColor: 'white',
                label: { show: true, color: 'white' }
              },
              emphasis: {
                areaColor: '#A5DABB'
              }
            },
            zoom: 1.2,
            data: mapData
          }
        ]
      };
    });
  }

  loadData() {
    this.webProvincesGet()
      .pipe(
        switchMap(() => {
          return this.provincesPost(this.content);
        })
      )
      .subscribe(() => {
        this.getProvinces();
      });
  }

  webProvincesGet() {
    return this.epidemicService.webProvincesGet().pipe(
      tap(data => {
        console.log(data);
        const content: Content = {
          content: JSON.stringify(data)
        };
        this.content = content;
        console.log(this.content);
      })
    );
  }

  provincesPost(data) {
    return this.epidemicService.provincesPost(data).pipe(
      tap(res => {
        console.log(res);
      })
    );
  }

  getProvinces() {
    this.epidemicService.provincesGet().subscribe(res => {
      const provincesList = JSON.parse(res.data.pop().content).newslist;
      const mapDataList = [];
      provincesList.forEach(p => {
        const data = {
          name: p.provinceShortName,
          value: p.currentConfirmedCount
        };
        mapDataList.push(data);
      });
      this.mapDataList = mapDataList;
      this.displayProvincesList = provincesList;
      console.log(provincesList);
      this.loadMap(mapDataList);
    });
  }

  getNews() {
    this.epidemicService.webNewsGet().subscribe(res => {
      console.log(res);
      const newsList = res.newslist[0].news;
      const desc = res.newslist[0].desc;
      console.log(newsList);
      console.log(desc);
      this.displayNewsList = newsList;
      this.displayDesc = desc;

      this.currentConfirmedCount = desc.currentConfirmedCount;
      this.confirmedCount = desc.confirmedCount;
      this.deadCount = desc.deadCount;
      this.curedCount = desc.curedCount;
      this.suspectedCount = desc.suspectedCount;
      this.seriousCount = desc.seriousCount;
    });
  }

  open(data) {
    console.log(data);
    window.open(data.sourceUrl);
  }
}
