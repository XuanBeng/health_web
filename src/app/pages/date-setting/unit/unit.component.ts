import { Component, OnInit } from '@angular/core';
import { NzMessageService, NzModalRef, NzModalService } from 'ng-zorro-antd';
import { Unit } from 'src/app/models/data-setting';
import { DataSettingService } from 'src/app/services/data-setting.service';
import { _searchFilter } from 'src/app/util/function-lib';

@Component({
  selector: 'app-unit',
  templateUrl: './unit.component.html',
  styleUrls: [
    './unit.component.less',
    '../date-setting/date-setting.component.less'
  ]
})
export class UnitComponent implements OnInit {
  unitList: Unit[]; // 从后台获取的ic卡数据list
  displayUnitList: Unit[]; // 用户显示的数据list

  editCache = []; // 编辑状态下的显示数据list
  searchString = ''; // 搜索框中的数值
  editStatus = false; // 当前是否处于新增/编辑状态

  // options = []; // 模态框下拉选择上级分类
  // optionValues = []; // 用于绑定option的值
  updateMode: 'edit' | 'add';

  // parentIdList: string[];
  // parentNameList: string[];
  deleteConfirmModal: NzModalRef;
  loading = false;

  constructor(
    private dataSettingService: DataSettingService,
    private message: NzMessageService,
    private modal: NzModalService
  ) {}

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.getUnit();
  }

  /**
   * 绑定点击编辑按钮事件
   * @param idx 编辑的索引
   */
  startEdit(idx: number): void {
    this.updateMode = 'edit';
    this.editStatus = true;
    this.editCache[idx].edit = true;
  }

  /**
   * 绑定取消按钮事件
   * @param idx 编辑的索引
   */
  cancelEdit(idx: string): void {
    this.editStatus = false;
    this.editCache[idx].edit = false;
    this.loadData();
  }

  /**
   * 打开新增编辑行
   */
  addEditCache() {
    this.updateMode = 'add';
    this.editStatus = true;
    if (this.editCache.length === 0 || !this.editCache[0].edit) {
      this.editCache = [
        {
          edit: true,
          id: '',
          name: '',
          grade: ''
        },
        ...this.editCache
      ];
      this.displayUnitList = this.editCache;
    }
  }

  // 搜索
  search() {
    setTimeout(() => {
      this.displayUnitList = this.unitList.filter(unit =>
        _searchFilter(this.searchString, unit, ['name'], 0)
      );
    }, 0);
  }

  /**
   * 打开删除确认模态框
   */
  showDeleteConfirm(data: Unit) {
    this.deleteConfirmModal = this.modal.confirm({
      nzTitle: `请确认是否删除 ${data.name}？`,
      nzClassName: 'delete-modal',
      nzOnOk: () => {
        this.deleteUnit(data.id);
      }
    });
  }

  /**
   * 查询
   */
  getUnit() {
    this.dataSettingService.unitGet().subscribe(res => {
      this.unitList = res.data;
      this.editCache = [];
      this.unitList.forEach(item => {
        item.edit = false;
        this.editCache.push(item);
      });
      this.displayUnitList = this.editCache;
    });
  }

  /**
   * 新增
   */
  addUnit(cache) {
    const unit: Unit = {
      id: cache.id,
      name: cache.name,
      grade: '1',
      edit: false
    };
    this.dataSettingService.unitPost(unit).subscribe(res => {
      console.log(res);
      if (res.errCode === 0) {
        this.editStatus = false;
        this.message.success(res.errMessage);
        this.loadData();
      } else {
        this.message.error(res.errMessage);
      }
    });
  }

  /**
   * 更新
   * @param id 主键
   */
  updateUnit(cache) {
    const unit: Unit = {
      id: cache.id,
      name: cache.name,
      grade: cache.grade,
      parentId: cache.parentId
    };
    this.dataSettingService.unitPut(unit).subscribe(res => {
      if (res.errCode === 0) {
        this.message.success(res.errMessage);
        this.editStatus = false;
        this.loadData();
      } else {
        this.message.error(res.errMessage);
      }
    });
  }

  /**
   * 删除
   * @param id 主键
   */
  deleteUnit(id: string) {
    this.dataSettingService.unitDelete(id).subscribe(res => {
      if (res.errCode === 0) {
        this.message.success(res.errMessage);
        this.loadData();
      }
    });
  }
}
