import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgZorroAntdModule } from 'ng-zorro-antd';

import { DateSettingRoutingModule } from './date-setting-routing.module';
import { DateSettingComponent } from './date-setting/date-setting.component';
import { UnitComponent } from './unit/unit.component';
import { UserComponent } from './user/user.component';

@NgModule({
  declarations: [DateSettingComponent, UnitComponent, UserComponent],
  imports: [
    CommonModule,
    NgZorroAntdModule,
    FormsModule,
    ReactiveFormsModule,
    DateSettingRoutingModule
  ]
})
export class DateSettingModule {}
