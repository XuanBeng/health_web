import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DateSettingComponent } from './date-setting/date-setting.component';
import { UnitComponent } from './unit/unit.component';
import { UserComponent } from './user/user.component';

const routes: Routes = [
  {
    path: '',
    component: DateSettingComponent,
    children: [
      { path: '', redirectTo: 'unit', pathMatch: 'full' },
      { path: 'unit', component: UnitComponent },
      { path: 'user', component: UserComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DateSettingRoutingModule {}
