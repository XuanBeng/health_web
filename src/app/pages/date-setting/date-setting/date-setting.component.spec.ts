import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DateSettingComponent } from './date-setting.component';

describe('DateSettingComponent', () => {
  let component: DateSettingComponent;
  let fixture: ComponentFixture<DateSettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DateSettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DateSettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
