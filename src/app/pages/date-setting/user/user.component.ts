import { Component, OnInit } from '@angular/core';
import { NzMessageService, NzModalRef, NzModalService } from 'ng-zorro-antd';
import { switchMap, tap } from 'rxjs/operators';
import { Unit } from 'src/app/models/data-setting';
import { User } from 'src/app/models/user';
import { DataSettingService } from 'src/app/services/data-setting.service';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { LoginService } from 'src/app/services/login.service';
import { _searchFilter } from 'src/app/util/function-lib';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: [
    './user.component.less',
    '../date-setting/date-setting.component.less'
  ]
})
export class UserComponent implements OnInit {
  userList: User[]; // 从后台获取的ic卡数据list
  displayUserList: User[]; // 用户显示的数据list

  unitList: Unit[];

  editCache = []; // 编辑状态下的显示数据list
  searchString = ''; // 搜索框中的数值
  editStatus = false; // 当前是否处于新增/编辑状态
  updateMode: 'edit' | 'add';
  // options = []; // 模态框下拉选择上级分类
  // optionValues = []; // 用于绑定option的值
  selectedValue: string;
  // parentIdList: string[];
  // parentNameList: string[];
  deleteConfirmModal: NzModalRef;
  loading = false;

  user: User;

  constructor(
    private loginService: LoginService,
    private localStorageService: LocalStorageService,
    private dataSettingService: DataSettingService,
    private message: NzMessageService,
    private modal: NzModalService
  ) {}

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.getUserInfo()
      .pipe(
        switchMap(() => {
          return this.getUser();
        })
      )
      .subscribe(() => {
        this.getUnit();
      });
  }

  /**
   * 绑定点击编辑按钮事件
   * @param idx 编辑的索引
   */
  startEdit(idx: number): void {
    this.updateMode = 'edit';
    this.editStatus = true;
    this.editCache[idx].edit = true;
  }

  /**
   * 绑定取消按钮事件
   * @param idx 编辑的索引
   */
  cancelEdit(idx: string): void {
    this.editStatus = false;
    this.editCache[idx].edit = false;
    this.loadData();
  }

  // 搜索
  search() {
    setTimeout(() => {
      this.displayUserList = this.userList.filter(unit =>
        _searchFilter(this.searchString, unit, ['username', 'name'], 0)
      );
    }, 0);
  }

  /**
   * 打开删除确认模态框
   */
  showDeleteConfirm(data) {
    this.deleteConfirmModal = this.modal.confirm({
      nzTitle: `请确认是否删除 ${data.name}？`,
      nzClassName: 'delete-modal',
      nzOnOk: () => {
        this.deleteUser(data.id);
      }
    });
  }

  getUnit() {
    this.dataSettingService.unitGet().subscribe(res => {
      this.unitList = res.data;
      console.log('result-unitList', this.unitList);
    });
  }

  /**
   * User查询
   */
  getUser() {
    const type = '0';
    return this.loginService.userGet(this.user.unit.id, type).pipe(
      tap(res => {
        this.userList = res.data;
        this.editCache = [];
        this.userList.forEach(item => {
          item.edit = false;
          item.unit_id = item.unit.id;
          item = this.convert(item);
          this.editCache.push(item);
        });
        this.displayUserList = this.editCache;
        console.log('result-displayUserList', this.displayUserList);
      })
    );
  }

  getUserInfo() {
    const userId = this.localStorageService.getItem('userId');
    return this.loginService.userGetById(userId).pipe(
      tap(res => {
        this.user = res.data[0];
        console.log(this.user);
      })
    );
  }

  /**
   * 更新
   * @param id 主键
   */
  updateUnit(cache) {
    console.log('result-cache', cache);
    const user: User = {
      id: cache.id,
      name: cache.name,
      username: cache.username,
      unit_id: cache.unit_id,
      type: cache.type,
      student_no: cache.username,
      mobile: cache.mobile
    };
    console.log('result-user', user);
    this.loginService.userPut(user).subscribe(res => {
      if (res.errCode === 0) {
        this.message.success(res.errMessage);
        this.editStatus = false;
        this.loadData();
      } else {
        this.message.error(res.errMessage);
      }
    });
  }

  /**
   * 删除
   * @param id 主键
   */
  deleteUser(id: string) {
    this.loginService.userDelete(id).subscribe(res => {
      if (res.errCode === 0) {
        this.message.success(res.errMessage);
        this.loadData();
      }
    });
  }

  // 转换type
  convert(user: User) {
    if (user.type === '0') {
      user.typeName = '普通';
    } else if (user.type === '1') {
      user.typeName = '管理';
    }
    return user;
  }
}
