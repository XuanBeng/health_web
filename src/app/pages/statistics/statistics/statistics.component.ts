import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { DailyEntry } from 'src/app/models/daily-entry';
import { User } from 'src/app/models/user';
import { DailyEntryService } from 'src/app/services/daily-entry.service';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.less'],
  providers: [DatePipe]
})
export class StatisticsComponent implements OnInit {
  user: User;
  allUserList: User[];
  dailyEntryList: DailyEntry[];

  displayUnfinishUserList: User[];
  displayAbnormalList: DailyEntry[];

  finishNo: number;
  unfinishNo: number;
  total: number;

  hotNo: number;
  breathNo: number;
  coughNo: number;

  // option = {};
  // xAxis = []; // 横坐标数据
  // data = [];
  // loading = true;

  constructor(
    private localStorageService: LocalStorageService,
    private dailyEntryService: DailyEntryService,
    private loginService: LoginService,
    private datePipe: DatePipe
  ) {}

  ngOnInit() {
    this.loadData();
  }

  /**
   * 加载数据
   */
  loadData() {
    this.getUserInfo()
      .pipe(
        switchMap(() => {
          return forkJoin(this.getUserList(), this.getDailyEntry());
        })
      )
      .subscribe(() => {
        this.getUnfinishUserList();
        this.getAbnormalLsit();
      });
  }
  /**
   * 获取当日打卡数据
   */
  getDailyEntry() {
    const startDate = this.datePipe.transform(
      new Date().setHours(0, 0, 0, 0),
      'yyyy-MM-dd HH:mm:ss'
    );
    const endDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd HH:mm:ss');
    return this.dailyEntryService
      .dailyEntryGetByUnit(startDate, endDate, this.user.unit.id)
      .pipe(
        tap(res => {
          this.dailyEntryList = res.data;
        })
      );
  }

  /**
   * 获取该单位下所有用户
   */
  getUserList() {
    const type = '0';
    return this.loginService.userGet(this.user.unit.id, type).pipe(
      tap(res => {
        this.allUserList = res.data;
        this.total = this.allUserList.length;
      })
    );
  }

  /**
   * 获取用户
   */
  getUserInfo() {
    const userId = this.localStorageService.getItem('userId');
    return this.loginService.userGetById(userId).pipe(
      tap(res => {
        this.user = res.data[0];
      })
    );
  }

  /**
   * 获取未完成的用户List
   */
  getUnfinishUserList() {
    const unfinishUserList = [];
    const userlist = this.allUserList;
    userlist.forEach(user => {
      user.finish = false;
      this.dailyEntryList.forEach(daliyEntry => {
        if (user.id === daliyEntry.user) {
          user.finish = true;
        }
      });
      if (!user.finish) {
        unfinishUserList.push(user);
      }
    });
    this.unfinishNo = unfinishUserList.length;
    this.finishNo = this.total - this.unfinishNo;
    this.displayUnfinishUserList = unfinishUserList;
  }

  /**
   * 获取异常List
   */
  getAbnormalLsit() {
    const abnormalList = [];
    let hotNo = 0;
    let coughNo = 0;
    let breathNo = 0;
    console.log('this.dailyEntryList', this.dailyEntryList);
    this.dailyEntryList.forEach(dailyEntry => {
      console.log('temperature', +dailyEntry.temperature);
      if (
        dailyEntry.if_cough === '1' ||
        +dailyEntry.temperature >= 37 ||
        dailyEntry.breath_short === '1'
      ) {
        abnormalList.push(dailyEntry);
        if (dailyEntry.if_cough === '1') {
          coughNo += 1;
        }
        if (dailyEntry.breath_short === '1') {
          breathNo += 1;
        }
        if (+dailyEntry.temperature >= 37) {
          hotNo += 1;
        }
      }
    });
    console.log('abnormalList', abnormalList);
    const abnormalUserList = [];
    abnormalList.forEach(d => {
      this.allUserList.forEach(user => {
        if (d.user === user.id) {
          d.usrClass = user;
        }
      });
      d = this.convertNode(d);
      if (d.usrClass) {
        abnormalUserList.push(d);
      }
    });
    console.log('abnormalUserList', abnormalUserList);
    this.hotNo = hotNo;
    this.coughNo = coughNo;
    this.breathNo = breathNo;
    this.displayAbnormalList = abnormalUserList;
    console.log('this.displayAbnormalList', this.displayAbnormalList);
  }

  /**
   * 赋值
   * @param item DailyEntry
   */
  convertNode(item: DailyEntry) {
    if (item.if_cough === '1') {
      item.if_cough_n = '有';
    } else {
      item.if_cough_n = '无';
    }
    if (item.breath_short === '1') {
      item.breath_short_n = '有';
    } else {
      item.breath_short_n = '无';
    }
    return item;
  }

  // updateEchartsOption(data) {
  //   this.data = data;
  //   console.log(data);

  //   this.xAxis = data.map(d => d.consignor.name);
  //   const echartData = {
  //     entQuantity: [],
  //     delQuantity: [],
  //     feePaid: [],
  //     feeToPay: [],
  //     toCharge: []
  //   };
  //   data.forEach(d => {
  //     echartData.entQuantity.push(d.ent_weight),
  //       echartData.delQuantity.push(d.del_weight),
  //       echartData.feePaid.push(d.fee_paid),
  //       echartData.feeToPay.push(d.fee_to_pay);
  //     echartData.toCharge.push(d.to_charge);
  //   });
  //   this.updateOptions(echartData);
  // }

  // updateOptions(echartData) {
  //   this.option = {
  //     tooltip: {
  //       trigger: 'axis',
  //       axisPointer: {
  //         // 坐标轴指示器，坐标轴触发有效
  //         type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
  //       }
  //     },
  //     legend: {
  //       data: ['入库量', '出库量', '未结营业额', '已收营业额', '未收营业额']
  //     },
  //     grid: {
  //       left: '3%',
  //       right: '4%',
  //       bottom: '3%',
  //       containLabel: true
  //     },
  //     xAxis: [
  //       {
  //         type: 'category',
  //         data: this.xAxis
  //       }
  //     ],
  //     yAxis: [
  //       {
  //         type: 'value'
  //       }
  //     ],
  //     series: [
  //       {
  //         name: '入库量',
  //         type: 'bar',
  //         data: echartData.entQuantity
  //       },
  //       {
  //         name: '出库量',
  //         type: 'bar',
  //         data: echartData.delQuantity
  //       },
  //       {
  //         name: '已收营业额',
  //         type: 'bar',
  //         stack: '营业额',
  //         data: echartData.feePaid
  //       },
  //       {
  //         name: '未收营业额',
  //         type: 'bar',
  //         stack: '营业额',
  //         data: echartData.feeToPay
  //       },
  //       {
  //         name: '未结营业额',
  //         type: 'bar',
  //         stack: '营业额',
  //         data: echartData.toCharge
  //       }
  //     ]
  //   };
  //   this.loading = false;
  // }
}
