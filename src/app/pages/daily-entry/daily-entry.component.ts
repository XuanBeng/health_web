import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzMessageService } from 'ng-zorro-antd';
import { switchMap, tap } from 'rxjs/operators';
import { DailyEntry } from 'src/app/models/daily-entry';
import { User } from 'src/app/models/user';
import { DailyEntryService } from 'src/app/services/daily-entry.service';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-daily-entry',
  templateUrl: './daily-entry.component.html',
  styleUrls: ['./daily-entry.component.less'],
  providers: [DatePipe]
})
export class DailyEntryComponent implements OnInit {
  validateForm: FormGroup;
  displayDaliyEntry: DailyEntry;

  ifFinish = false;
  ifSuccess = false;
  user: User;

  constructor(
    private fb: FormBuilder,
    private localStorageService: LocalStorageService,
    private dailyEntryService: DailyEntryService,
    private loginService: LoginService,
    private message: NzMessageService,
    private datePipe: DatePipe
  ) {}

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      temperature: [null, [Validators.required]],
      if_cough: [null, [Validators.required]],
      breath_short: [null, [Validators.required]],
      other: [null, [Validators.required]]
    });

    this.getUserInfo()
      .pipe(
        switchMap(() => {
          return this.getDailyEntryIfFinish();
        })
      )
      .subscribe();
  }

  /**
   * 提交每日申报
   */
  submitForm(): void {
    // tslint:disable-next-line: forin
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
    console.log('validateForm', this.validateForm);
    console.log('user', this.user);
    if (this.validateForm.valid) {
      const dailyEntry = {
        user: this.user.id,
        if_cough: this.validateForm.value.if_cough,
        temperature: this.validateForm.value.temperature,
        breath_short: this.validateForm.value.breath_short,
        other: this.validateForm.value.other,
        unit: this.user.unit.id
      };
      this.displayDaliyEntry = this.convertNode(dailyEntry);
      this.dailyEntryService.dailyEntryPost(dailyEntry).subscribe(res => {
        if (res.errCode === 0) {
          this.message.success(res.errMessage);
          this.ifFinish = true;
        } else {
          this.message.error(res.errMessage);
        }
      });
    }
  }

  /**
   * 获取该用户是否完成当日申报
   */
  getDailyEntryIfFinish() {
    const startDate = this.datePipe.transform(
      new Date().setHours(0, 0, 0, 0),
      'yyyy-MM-dd HH:mm:ss'
    );
    const endDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd HH:mm:ss');
    console.log(startDate, endDate);
    return this.dailyEntryService
      .dailyEntryGet(startDate, endDate, this.user.id)
      .pipe(
        tap(res => {
          if (res.data.length !== 0) {
            console.log(res);
            const list = res.data.map(item => {
              return this.convertNode(item);
            });
            this.displayDaliyEntry = list.pop();
            this.ifFinish = true;
          } else {
            this.ifFinish = false;
          }
        })
      );
  }

  /**
   * 获取用户信息
   */
  getUserInfo() {
    const userId = this.localStorageService.getItem('userId');
    return this.loginService.userGetById(userId).pipe(
      tap(res => {
        this.user = res.data[0];
        console.log(this.user);
      })
    );
  }

  /**
   * 赋值
   * @param item 每日申报
   */
  convertNode(item: DailyEntry) {
    if (item.if_cough === '1') {
      item.if_cough_n = '有';
    } else {
      item.if_cough_n = '无';
    }
    if (item.breath_short === '1') {
      item.breath_short_n = '有';
    } else {
      item.breath_short_n = '无';
    }
    return item;
  }
}
