import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.less']
})
export class ShellComponent implements OnInit {
  isCollapsed = false;
  userName: string;
  type: any;

  constructor(
    private localStorageService: LocalStorageService,
    private loginService: LoginService
  ) {}

  ngOnInit() {
    this.getUserInfo();
  }

  getUserInfo() {
    this.userName = this.localStorageService.getItem('userName');
    this.type = this.localStorageService.getItem('type');
    console.log('this.type', this.type);
  }

  logout() {
    this.loginService.logout();
  }
}
