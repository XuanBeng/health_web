import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DailyEntryComponent } from '../daily-entry/daily-entry.component';
import { EpidemicComponent } from '../epidemic/epidemic.component';
import { ShellComponent } from './shell.component';

const routes: Routes = [
  {
    path: '',
    component: ShellComponent,
    // canActivate: [AuthGuard],
    // canActivateChild: [AuthGuard],
    children: [
      { path: '', redirectTo: 'daily-entry', pathMatch: 'full' },
      { path: 'daily-entry', component: DailyEntryComponent },
      { path: 'epidemic', component: EpidemicComponent },
      {
        path: 'statistics',
        loadChildren: '../statistics/statistics.module#StatisticsModule'
      },
      {
        path: 'date-setting',
        loadChildren: '../date-setting/date-setting.module#DateSettingModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShellRoutingModule {}
