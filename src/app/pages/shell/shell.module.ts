import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { NgxEchartsModule } from 'ngx-echarts';

import { DailyEntryComponent } from '../daily-entry/daily-entry.component';
import { EpidemicComponent } from '../epidemic/epidemic.component';
import { ShellRoutingModule } from './shell-routing.module';
import { ShellComponent } from './shell.component';

@NgModule({
  imports: [
    CommonModule,
    NgZorroAntdModule,
    FormsModule,
    ReactiveFormsModule,
    NgxEchartsModule,
    ShellRoutingModule
  ],
  declarations: [ShellComponent, DailyEntryComponent, EpidemicComponent]
})
export class ShellModule {}
