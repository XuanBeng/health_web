import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { loginUrl, userUrl } from '../base-url';
import { Result } from '../models/result';
import { User } from '../models/user';
import { LocalStorageService } from './local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  constructor(
    private http: HttpClient,
    private localStorageService: LocalStorageService,
    private router: Router
  ) {}

  login(username: string, password: string) {
    return this.http
      .post<Result<any>>(loginUrl, { username, password })
      .pipe(
        map(result => {
          console.log(result);
          this.localStorageService.setItem('userName', result.data.username);
          this.localStorageService.setItem('tokenKey', result.data.token);
          this.localStorageService.setItem('userId', result.data.id);
          this.localStorageService.setItem('type', result.data.type);
          return result;
        })
      );
  }

  // 注册
  register(user: User) {
    return this.http.post<Result<any>>(userUrl, user);
  }

  logout() {
    this.localStorageService.removeItem('userName');
    this.localStorageService.removeItem('tokenKey');
    this.localStorageService.removeItem('userId');
    this.router.navigate(['/login']);
  }

  // 更新unit接口
  userPut(user: User) {
    const id = user.id;
    return this.http.put<Result<any>>(userUrl + id + '/', user);
  }

  // 删除unit接口
  userDelete(id: string) {
    return this.http.delete<Result<any>>(userUrl + id + '/');
  }

  // 查询unit接口
  userGet(unitId?: string, type?: string) {
    let params = new HttpParams();
    params = params.set('unit_id', unitId);
    params = params.set('type', type);
    return this.http.get<Result<User[]>>(userUrl, { params });
  }

  userGetById(id: string): Observable<Result<User>> {
    let params = new HttpParams();
    params = params.set('id', id);
    return this.http.get<Result<User>>(userUrl, { params });
  }
}
