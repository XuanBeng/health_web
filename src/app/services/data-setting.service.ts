import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { unitUrl } from '../base-url';
import { Unit } from '../models/data-setting';
import { Result } from '../models/result';

@Injectable({
  providedIn: 'root'
})
export class DataSettingService {
  constructor(private http: HttpClient) {}

  unitPost(unit: Unit) {
    return this.http.post<Result<any>>(unitUrl, unit);
  }
  // 更新unit接口
  unitPut(unit: Unit) {
    const id = unit.id;
    return this.http.put<Result<any>>(unitUrl + id + '/', unit);
  }

  // 删除unit接口
  unitDelete(id: string) {
    return this.http.delete<Result<any>>(unitUrl + id + '/');
  }
  // 查询unit接口
  unitGet() {
    return this.http.get<Result<Unit[]>>(unitUrl);
  }
}
