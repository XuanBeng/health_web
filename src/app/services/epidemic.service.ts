import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { newsUrl, provincesUrl } from '../base-url';
import { Content, Result } from '../models/result';

@Injectable({
  providedIn: 'root'
})
export class EpidemicService {
  provincesWebUrl =
    'http://api.tianapi.com/txapi/ncovcity/index?key=659cd1f1aeaeb68f79df8ff1968566a7';
  newsWebUrl =
    'http://api.tianapi.com/txapi/ncov/index?key=659cd1f1aeaeb68f79df8ff1968566a7';
  constructor(private http: HttpClient) {}

  provincesPost(data: Content) {
    return this.http.post<Result<any>>(provincesUrl, data);
  }

  provincesGet() {
    return this.http.get<Result<Content[]>>(provincesUrl);
  }

  newsPost(data: Content) {
    return this.http.post<Result<any>>(newsUrl, data);
  }

  newsGet() {
    return this.http.get<Result<Content[]>>(newsUrl);
  }

  webProvincesGet() {
    return this.http.get<Result<any>>(this.provincesWebUrl);
  }

  webNewsGet() {
    return this.http.get<any>(this.newsWebUrl);
  }
}
