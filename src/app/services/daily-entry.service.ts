import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { daliyEntryUrl } from '../base-url';
import { DailyEntry } from '../models/daily-entry';
import { Result } from '../models/result';

@Injectable({
  providedIn: 'root'
})
export class DailyEntryService {
  constructor(private http: HttpClient) {}

  dailyEntryPost(daliyEntry: any) {
    return this.http.post<Result<any>>(daliyEntryUrl, daliyEntry);
  }

  dailyEntryGet(startDate, endDate, userId?: string) {
    let params = new HttpParams();
    if (userId) {
      params = params.set('user', userId);
    }
    params = params.set('start_date', startDate);
    params = params.set('end_date', endDate);
    return this.http.get<Result<DailyEntry[]>>(daliyEntryUrl, { params });
  }

  dailyEntryGetByUnit(startDate, endDate, unitId?: string) {
    let params = new HttpParams();
    if (unitId) {
      params = params.set('unit', unitId);
    }
    params = params.set('start_date', startDate);
    params = params.set('end_date', endDate);
    return this.http.get<Result<DailyEntry[]>>(daliyEntryUrl, { params });
  }
}
