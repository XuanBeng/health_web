#! /bin/bash

ng build --prod
docker build -t harbor.smuport.com/library/ctas-designer-web .
docker push harbor.smuport.com/library/ctas-designer-web
